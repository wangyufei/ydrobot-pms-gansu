package com.ydrobot.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.dto.PointDto;
import com.ydrobot.pms.model.PowerHistory;

@Mapper
@Component
public interface PowerHistoryMapper {
	
	/**
	 * 获取设备实物id数
	 * @param deviceId
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select count(*) from power_history as ph "
			+ "left join device_property_default as dpd on ph.id=dpd.id "
			+ "left join device as d on dpd.device_id = d.id "
			+ "where d.id = #{deviceId} and task_id = #{taskHistoryId} and pms_entity_status = 1")
	int getDevieSwidNum(@Param("deviceId") Integer deviceId, @Param("taskHistoryId") Integer taskHistoryId);
	
	/**
	 * 通过点位id和任务执行记录获取点位上报记录信息
	 * @param pointId
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select camera_pic as cameraPic,flir_pic as flirPic from power_history "
			+ "where id = #{powerHistoryId} and task_id = #{taskHistoryId}")
	PowerHistory getPowerHistoryByPointIdAndTaskHistoryId(@Param("powerHistoryId") Integer powerHistoryId, @Param("taskHistoryId") Integer taskHistoryId);
	
	/**
	 * 获取点位信息
	 * @param pointId
	 * @return
	 */
	@Select("select id, save_type as saveTypeId from device_property_default where id = #{pointId}")
	PointDto getPointById(Integer pointId);
}
