package com.ydrobot.pms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.TaskDef;

@Mapper
@Component
public interface TaskDefMapper {

	/**
	 * 通过变电站id获取任务列表
	 * @param pmsBdzId
	 * @return
	 */
	@Select("select td.task_name as taskName,td.task_id as taskId from task_def as td left join robot_def as rd on td.robot_id=rd.robot_id left join device as d on rd.station_id = d.id where d.pms_device_id = #{pmsBdzId}")
	List<TaskDef> getTaskByPmsBdzId(String pmsBdzId);
	
	/**
	 * 通过任务id获取任务信息
	 * @param taskId
	 * @return
	 */
	@Select("select task_id as taskId,task_name as taskName from task_def where task_id = #{taskId}")
	TaskDef getTaskByTaskId(Integer taskId);
}
