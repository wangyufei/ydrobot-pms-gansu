package com.ydrobot.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.TaskRegular;

@Mapper
@Component
public interface TaskRegularMapper {

	/**
	 * 添加定期执行任务
	 * @param taskRegular
	 * @return
	 */
	@Select("INSERT INTO task_regular(task_id, execute_time, pms_jh_id) VALUES(#{taskId}, #{executeTime},#{pmsJhId})")
	void insert(TaskRegular taskRegular);
}
