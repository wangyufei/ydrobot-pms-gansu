package com.ydrobot.pms.mapper;

import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.TaskHistory;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
@Component
public interface TaskHistoryMapper {

	/**
	 * 通过id获取任务执行记录信息
	 * @param id
	 * @return
	 */
	@Select("select id,pms_jh_id as pmsJhId,task_id as taskId from task_history where id = #{id}")
	TaskHistory getTaskHistoryById(Integer id);
}
