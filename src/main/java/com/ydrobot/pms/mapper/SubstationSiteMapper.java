package com.ydrobot.pms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.SubstationSite;

@Mapper
@Component
public interface SubstationSiteMapper {
	
	/**
	 * 通过pms变电站id获取站点信息
	 * @param pmsBdzId
	 * @return
	 */
	@Select("select id,name,pms_bdz_id as pmsBdzId from substation_site where pms_bdz_id = #{pmsBdzId}")
	SubstationSite getStationInfo(String pmsBdzId);
	
	/**
	 * 通过任务id获取变电站信息
	 * @param taskId
	 * @return
	 */
	@Select("select s.name,s.pms_bdz_id as pmsBdzId from substation_site as s "
			+ "left join robot_def as rd on s.id = rd.station_id "
			+ "left join task_def as td on td.robot_id = rd.robot_id "
			+ "where td.task_id = #{taskId}")
	SubstationSite getStationInfoByTaskId(Integer taskId);
	
}
