package com.ydrobot.pms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.DevicePropertyDefault;

@Mapper
@Component
public interface DevicePropertyDefaultMapper {
	
	/**
	 * 获取当前任务设备列表
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select dpd.id,dpd.device_name as deviceName,dpd.pms_device_id as pmsDeviceId, dpd.pms_entity_id as pmsEntityId from device_property_default as dpd "
			+ "left join power_history as ph on dpd.id = ph.id "
			+ "where ph.task_id = #{taskHistoryId} "
			+ "group by dpd.pms_device_id")
	List<DevicePropertyDefault> getCurrentTaskDeviceList(Integer taskHistoryId);

	/**
	 * 通过点位id获取设备信息
	 * @param pointId
	 * @return
	 */
	@Select("select id,device_name as deviceName,pms_device_id as pmsDeviceId, pms_entity_id as pmsEntityId from device_property_default "
			+ "where id = #{pointId}")
	DevicePropertyDefault getDeviceByPointId(Integer pointId);
}
