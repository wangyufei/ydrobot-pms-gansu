package com.ydrobot.pms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.ydrobot.pms.model.PointAlarm;

@Mapper
@Component
public interface PointAlarmMapper {

	/**
	 * 获取某个任务执行记录下的报警数
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select count(*) from point_alarm as pa "
			+ "left join power_history as ph on pa.power_history_rowid = ph.id "
			+ "where task_id = #{taskHistoryId}")
	int getAlarmCountByTaskHistoryId(Integer taskHistoryId);
	
	/**
	 * 通过任务记录id获取点位报警记录
	 * 
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select pa.id,pa.power_history_rowid as powerHistoryRowid,pa.alarm_level_id as alarmLevelId, pa.alarm_desc as alarmDesc from point_alarm as pa "
			+ "left join power_history as ph on pa.power_history_rowid = ph.id "
			+ "where task_id = #{taskHistoryId}")
	List<PointAlarm> getPointAlarmByTaskHistoryId(Integer taskHistoryId);
}
