package com.ydrobot.pms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class YdrobotPmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(YdrobotPmsApplication.class, args);
	}

}
