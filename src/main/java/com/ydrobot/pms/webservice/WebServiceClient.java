package com.ydrobot.pms.webservice;

import java.net.URL;

import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.transport.http.HttpTransport;

import com.alibaba.fastjson.JSONObject;
import com.ydrobot.pms.utils.ProjectPropUtils;

public class WebServiceClient {

    public Object req(String method, JSONObject jsonObject) {
        Object[] results = null;
        try {
            Client client = new Client(new URL(ProjectPropUtils.get("pms.ws.url")));
            client.setProperty("mtom-enable", "true");
            client.setProperty(HttpTransport.CHUNKING_ENABLED, "true");
            results = client.invoke(method, new Object[] { jsonObject.toString() });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results[0];
    }
}
