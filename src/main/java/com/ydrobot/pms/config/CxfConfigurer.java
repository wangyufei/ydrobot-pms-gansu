package com.ydrobot.pms.config;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ydrobot.pms.service.RobotApiService;

@Configuration
public class CxfConfigurer {

    @Autowired
    private RobotApiService robotApiService;

    @Bean
    public ServletRegistrationBean disServlet() {
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(),
                "/service/*");
        return servletRegistrationBean;
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), robotApiService);
        endpoint.publish("/robotApiService");
        return endpoint;
    }
}
