package com.ydrobot.pms.request;

public class SyncTaskStatusParam {
	private Integer taskHistoryId;

	public Integer getTaskHistoryId() {
		return taskHistoryId;
	}

	public void setTaskHistoryId(Integer taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}
	
	
}
