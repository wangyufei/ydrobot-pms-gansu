package com.ydrobot.pms.zmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import com.ydrobot.pms.request.SyncTaskStatusParam;
import com.ydrobot.pms.service.RobotApiService;

@Component
public class ZmqRep implements ApplicationRunner{

	private static final Logger log = LoggerFactory.getLogger(ZmqRep.class);
	
	@Autowired
	private RobotApiService robotApiService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("zmq订阅服务启动成功");
		ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket socket = context.socket(SocketType.REP);
        socket.bind("tcp://*:11319");
        while (true) {
            byte[] request = socket.recv();
            if (new String(request).equals("END")) {
                break;
            }
            log.info("Response recv:\t" + new String(request));
            
            String response = "success!";
            socket.send(response.getBytes());

            SyncTaskStatusParam syncTaskStatusParam = new SyncTaskStatusParam();
            syncTaskStatusParam.setTaskHistoryId(Integer.valueOf(new String(request)));
            robotApiService.syncTaskStatus(syncTaskStatusParam);
        }
        //关闭
        socket.close();
        context.term();
	}

}
