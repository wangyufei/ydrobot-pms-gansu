package com.ydrobot.pms.zmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import com.ydrobot.pms.request.SyncTaskStatusParam;
import com.ydrobot.pms.service.RobotApiService;

@Component
public class ZmqReq{

	private static final Logger log = LoggerFactory.getLogger(ZmqReq.class);
	
	public static void main(String args[]) {
		String response = "";
		ZMQ.Context context = ZMQ.context(1);
	    ZMQ.Socket socket = context.socket(SocketType.REQ);
	    socket.setSendTimeOut(1000);
	    socket.setReceiveTimeOut(3000);
	    if (socket.connect("tcp://127.0.0.1:11319")) {
	        socket.send("1".getBytes(), 0);
	        byte[] reply = socket.recv(0);
	        response = new String(reply);
	        log.info("上位机响应结果:" + response);
	        socket.close();
	    } else {
	        log.info("连接上位机ZMQ服务超时");
	    }
	    
	}
}
