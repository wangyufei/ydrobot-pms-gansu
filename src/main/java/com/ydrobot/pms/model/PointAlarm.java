package com.ydrobot.pms.model;

import javax.persistence.Column;

public class PointAlarm {
	
	private Integer id;
	
	@Column(name = "power_history_rowid")
	private Integer powerHistoryRowid;
	
	@Column(name = "alarm_level_id")
	private Integer alarmLevelId;
	
	@Column(name = "alarm_setting_id")
	private Integer alarmSettingId;
	
	@Column(name = "alarm_desc")
	private String alarmDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPowerHistoryRowid() {
		return powerHistoryRowid;
	}

	public void setPowerHistoryRowid(Integer powerHistoryRowid) {
		this.powerHistoryRowid = powerHistoryRowid;
	}

	public Integer getAlarmLevelId() {
		return alarmLevelId;
	}

	public void setAlarmLevelId(Integer alarmLevelId) {
		this.alarmLevelId = alarmLevelId;
	}

	public Integer getAlarmSettingId() {
		return alarmSettingId;
	}

	public void setAlarmSettingId(Integer alarmSettingId) {
		this.alarmSettingId = alarmSettingId;
	}

	public String getAlarmDesc() {
		return alarmDesc;
	}

	public void setAlarmDesc(String alarmDesc) {
		this.alarmDesc = alarmDesc;
	}
	
	
}
