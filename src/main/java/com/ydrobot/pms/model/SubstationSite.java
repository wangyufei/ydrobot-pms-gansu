package com.ydrobot.pms.model;

public class SubstationSite {
	private Integer id;
	
	private String name;
	
	private Float x;
	
	private Float y;
	
	private Float z;
	
	private String pmsBdzId;
	
	private String remark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getX() {
		return x;
	}

	public void setX(Float x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}

	public Float getZ() {
		return z;
	}

	public void setZ(Float z) {
		this.z = z;
	}

	public String getPmsBdzId() {
		return pmsBdzId;
	}

	public void setPmsBdzId(String pmsBdzId) {
		this.pmsBdzId = pmsBdzId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
