package com.ydrobot.pms.model;

import java.util.Date;

import javax.persistence.Column;

public class TaskRegular {
	
	private Integer id;
	
	@Column(name = "task_id")
	private Integer taskId;
	
	@Column(name = "execute_time")
	private Date executeTime;
	
	@Column(name = "pms_jh_id")
	private String pmsJhId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Date getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Date executeTime) {
		this.executeTime = executeTime;
	}

	public String getPmsJhId() {
		return pmsJhId;
	}

	public void setPmsJhId(String pmsJhId) {
		this.pmsJhId = pmsJhId;
	}
	
}
