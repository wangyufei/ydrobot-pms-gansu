package com.ydrobot.pms.model;

import java.sql.Date;

import javax.persistence.Column;

public class TaskDef {
	
	@Column(name = "task_id")
	private Integer taskId;
	
	@Column(name = "task_name")
	private String taskName;
	
	@Column(name = "xj_type_id")
	private Integer xjTypeId;
	
	@Column(name = "check_status")
	private Integer checkStatus;
	
	@Column(name = "operator_id")
	private Integer operatorId;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "send_operator_id")
	private Integer sendOperatorId;
	
	@Column(name = "send_time")
	private Date sendTime;
	
	@Column(name = "plan_time")
	private Date planTime;
	
	@Column(name = "run_flag")
	private Integer runFlag;
	
	@Column(name = "cycle")
	private Integer cycle;
	
	@Column(name = "task_desc")
	private String taskDesc;
	
	@Column(name = "inspect_task_id")
	private Integer inspectTaskId;
	
	@Column(name = "bad_weather_id")
	private Integer badWeatherId;
	
	@Column(name = "end_time")
	private Date endTime;
	
	@Column(name = "robot_id")
	private Integer robotId;

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getXjTypeId() {
		return xjTypeId;
	}

	public void setXjTypeId(Integer xjTypeId) {
		this.xjTypeId = xjTypeId;
	}

	public Integer getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Integer checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Integer getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getSendOperatorId() {
		return sendOperatorId;
	}

	public void setSendOperatorId(Integer sendOperatorId) {
		this.sendOperatorId = sendOperatorId;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public Integer getRunFlag() {
		return runFlag;
	}

	public void setRunFlag(Integer runFlag) {
		this.runFlag = runFlag;
	}

	public Integer getCycle() {
		return cycle;
	}

	public void setCycle(Integer cycle) {
		this.cycle = cycle;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	public Integer getInspectTaskId() {
		return inspectTaskId;
	}

	public void setInspectTaskId(Integer inspectTaskId) {
		this.inspectTaskId = inspectTaskId;
	}

	public Integer getBadWeatherId() {
		return badWeatherId;
	}

	public void setBadWeatherId(Integer badWeatherId) {
		this.badWeatherId = badWeatherId;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getRobotId() {
		return robotId;
	}

	public void setRobotId(Integer robotId) {
		this.robotId = robotId;
	}
}
