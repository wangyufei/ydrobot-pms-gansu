package com.ydrobot.pms.model;

import java.util.Date;

import javax.persistence.Column;

public class PowerHistory {

	private Integer id;
	
	@Column(name = "record_datetime")
	private Date recordDatetime;
	
	@Column(name = "task_id")
	private Integer taskId;
	
	@Column(name = "camera_pic")
	private String cameraPic;
	
	@Column(name = "flir_pic")
	private String flirPic;
	
	@Column(name = "sound")
	private String sound;
	
	@Column(name = "recon_datetime")
	private Date reconDatetime;
	
	private Float value;
	
	@Column(name = "check_flag")
	private Integer checkFlag;
	
	@Column(name = "alarm_level_id")
	private Integer alarmLevelId;
	
	@Column(name = "alarm_desc")
	private String alarmDesc;
	
	@Column(name = "err_flag")
	private Integer errFlag;
	
	@Column(name = "is_audited")
	private Integer isAudited;
	
	@Column(name = "modify_value")
	private float modifyValue;
	
	@Column(name = "check_datetime")
	private Date checkDatetime;
	
	@Column(name = "advise_desc")
	private String adviseDesc;
	
	private Integer operater;
	
	@Column(name = "pms_entity_status")
	private String pmsEntityStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getRecordDatetime() {
		return recordDatetime;
	}

	public void setRecordDatetime(Date recordDatetime) {
		this.recordDatetime = recordDatetime;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getCameraPic() {
		return cameraPic;
	}

	public void setCameraPic(String cameraPic) {
		this.cameraPic = cameraPic;
	}

	public String getFlirPic() {
		return flirPic;
	}

	public void setFlirPic(String flirPic) {
		this.flirPic = flirPic;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public Date getReconDatetime() {
		return reconDatetime;
	}

	public void setReconDatetime(Date reconDatetime) {
		this.reconDatetime = reconDatetime;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Integer getCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(Integer checkFlag) {
		this.checkFlag = checkFlag;
	}

	public Integer getAlarmLevelId() {
		return alarmLevelId;
	}

	public void setAlarmLevelId(Integer alarmLevelId) {
		this.alarmLevelId = alarmLevelId;
	}

	public String getAlarmDesc() {
		return alarmDesc;
	}

	public void setAlarmDesc(String alarmDesc) {
		this.alarmDesc = alarmDesc;
	}

	public Integer getErrFlag() {
		return errFlag;
	}

	public void setErrFlag(Integer errFlag) {
		this.errFlag = errFlag;
	}

	public Integer getIsAudited() {
		return isAudited;
	}

	public void setIsAudited(Integer isAudited) {
		this.isAudited = isAudited;
	}

	public float getModifyValue() {
		return modifyValue;
	}

	public void setModifyValue(float modifyValue) {
		this.modifyValue = modifyValue;
	}

	public Date getCheckDatetime() {
		return checkDatetime;
	}

	public void setCheckDatetime(Date checkDatetime) {
		this.checkDatetime = checkDatetime;
	}

	public String getAdviseDesc() {
		return adviseDesc;
	}

	public void setAdviseDesc(String adviseDesc) {
		this.adviseDesc = adviseDesc;
	}

	public Integer getOperater() {
		return operater;
	}

	public void setOperater(Integer operater) {
		this.operater = operater;
	}

	public String getPmsEntityStatus() {
		return pmsEntityStatus;
	}

	public void setPmsEntityStatus(String pmsEntityStatus) {
		this.pmsEntityStatus = pmsEntityStatus;
	}
}
