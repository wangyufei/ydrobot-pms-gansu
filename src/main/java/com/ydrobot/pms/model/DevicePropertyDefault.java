package com.ydrobot.pms.model;

import javax.persistence.Column;

public class DevicePropertyDefault {
	
	private Integer id;
	
	@Column(name = "device_name")
	private String deviceName;
	
	@Column(name = "save_type")
	private Integer saveType;
	
	@Column(name = "pms_device_id")
	private String pmsDeviceId;
	
	@Column(name = "pms_entity_id")
	private String pmsEntityId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Integer getSaveType() {
		return saveType;
	}

	public void setSaveType(Integer saveType) {
		this.saveType = saveType;
	}

	public String getPmsDeviceId() {
		return pmsDeviceId;
	}

	public void setPmsDeviceId(String pmsDeviceId) {
		this.pmsDeviceId = pmsDeviceId;
	}

	public String getPmsEntityId() {
		return pmsEntityId;
	}

	public void setPmsEntityId(String pmsEntityId) {
		this.pmsEntityId = pmsEntityId;
	}
	
	
}
