package com.ydrobot.pms.model;

import java.util.Date;

import javax.persistence.Column;

public class TaskHistory {
	
	private Integer id;
	
	@Column(name = "task_id")
	private Integer taskId;
	
	@Column(name = "task_start_date")
	private Date taskStartDate;
	
	@Column(name = "task_end_date")
	private Date taskEndDate;
	
	@Column(name = "task_status")
	private Integer taskStatus;
	
	@Column(name = "check_status")
	private Integer checkStatus;
	
	@Column(name = "check_time")
	private Date checkTime;
	
	private float tmp;
	
	private float hum;
	
	private float wind;
	
	private String weather;
	
	@Column(name = "pms_jh_id")
	private String pmsJhId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Date getTaskStartDate() {
		return taskStartDate;
	}

	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	public Date getTaskEndDate() {
		return taskEndDate;
	}

	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	public Integer getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Integer getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Integer checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public float getTmp() {
		return tmp;
	}

	public void setTmp(float tmp) {
		this.tmp = tmp;
	}

	public float getHum() {
		return hum;
	}

	public void setHum(float hum) {
		this.hum = hum;
	}

	public float getWind() {
		return wind;
	}

	public void setWind(float wind) {
		this.wind = wind;
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}

	public String getPmsJhId() {
		return pmsJhId;
	}

	public void setPmsJhId(String pmsJhId) {
		this.pmsJhId = pmsJhId;
	}
	
}
