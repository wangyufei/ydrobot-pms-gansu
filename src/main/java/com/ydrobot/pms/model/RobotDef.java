package com.ydrobot.pms.model;

import javax.persistence.Column;

public class RobotDef {
	
	@Column(name = "robot_id")
	private Integer robotId;
	
	@Column(name = "robot_name")
	private String robotName;
	
	@Column(name = "station_id")
	private Integer stationId;
	
	@Column(name = "master_ip")
	private String masterIp;
	
	@Column(name = "master_port")
	private Integer masterPort;
	
	@Column(name = "server_ip")
	private String serverIp;
	
	@Column(name = "server_port")
	private Integer serverPort;

	public Integer getRobotId() {
		return robotId;
	}

	public void setRobotId(Integer robotId) {
		this.robotId = robotId;
	}

	public String getRobotName() {
		return robotName;
	}

	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public String getMasterIp() {
		return masterIp;
	}

	public void setMasterIp(String masterIp) {
		this.masterIp = masterIp;
	}

	public Integer getMasterPort() {
		return masterPort;
	}

	public void setMasterPort(Integer masterPort) {
		this.masterPort = masterPort;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public Integer getServerPort() {
		return serverPort;
	}

	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}
	
}
