package com.ydrobot.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ydrobot.pms.request.SyncTaskStatusParam;
import com.ydrobot.pms.service.RobotApiService;

@RestController
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private RobotApiService robotApiService;
	
	@PostMapping("/syncTaskStatus")
	public JSONObject syncTaskStatus(@RequestBody SyncTaskStatusParam params){
		robotApiService.syncTaskStatus(params);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("zxjg", "1");
		return jsonObject;
	}
}
