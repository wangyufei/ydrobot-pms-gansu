package com.ydrobot.pms.utils;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectPropUtils {
	
    private static final Logger log = LoggerFactory.getLogger(ProjectPropUtils.class);
    
    private static ResourceBundle bundle;

    public static String get(String code) {
        initBundle();
        if (!bundle.containsKey(code)) {
            return null;
        }
        return bundle.getString(code);
    }

    public static String get(String code, String df) {
        initBundle();
        if (!bundle.containsKey(code)) {
            return df;
        }
        return bundle.getString(code);
    }

    private static void initBundle() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("config", Locale.getDefault());
        }
    }

    public static void main(String args[]) {
        System.out.println(get("sc-api.userName"));
    }
}
