package com.ydrobot.pms.dto;

public class PointDto {
	
	/**
	 * 点位id
	 */
	private Integer id;
	
	/**
	 * 保存类型 1红外，2可见光，3视频
	 */
	private Integer saveTypeId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSaveTypeId() {
		return saveTypeId;
	}

	public void setSaveTypeId(Integer saveTypeId) {
		this.saveTypeId = saveTypeId;
	}
	
}
