package com.ydrobot.pms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ydrobot.pms.dto.PointDto;
import com.ydrobot.pms.mapper.DevicePropertyDefaultMapper;
import com.ydrobot.pms.mapper.PointAlarmMapper;
import com.ydrobot.pms.mapper.PowerHistoryMapper;
import com.ydrobot.pms.mapper.SubstationSiteMapper;
import com.ydrobot.pms.mapper.TaskDefMapper;
import com.ydrobot.pms.mapper.TaskHistoryMapper;
import com.ydrobot.pms.mapper.TaskRegularMapper;
import com.ydrobot.pms.model.DevicePropertyDefault;
import com.ydrobot.pms.model.PointAlarm;
import com.ydrobot.pms.model.PowerHistory;
import com.ydrobot.pms.model.SubstationSite;
import com.ydrobot.pms.model.TaskDef;
import com.ydrobot.pms.model.TaskHistory;
import com.ydrobot.pms.model.TaskRegular;
import com.ydrobot.pms.request.SyncTaskStatusParam;
import com.ydrobot.pms.service.RobotApiService;
import com.ydrobot.pms.utils.Base64Util;
import com.ydrobot.pms.webservice.WebServiceClient;

@WebService(serviceName = "RobotApiService", // 与接口中指定的name一致
        targetNamespace = "http://pms.ydrobot.com", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "com.ydrobot.pms.service.RobotApiService"// 接口地址
)
@Component
public class RobotApiServiceImpl implements RobotApiService {

	@Value("${upper.computer.ip}")
    private String upperComputerIp;
	
	@Value("${upper.computer.port}")
	private String upperComputerPort;
	
	@Value("${resource.path}")
	private String resourcePath;
	
    private static final Logger log = LoggerFactory.getLogger(RobotApiServiceImpl.class);

    @Autowired
    private SubstationSiteMapper substationSiteMapper;
    @Autowired
    private TaskDefMapper taskDefMapper;
    @Autowired
    private TaskRegularMapper taskRegularMapper;
    @Autowired
    private TaskHistoryMapper taskHistoryMapper;
    @Autowired
    private PointAlarmMapper pointAlarmMapper;
    @Autowired
    private PowerHistoryMapper powerHistoryMapper;
    @Autowired
    private DevicePropertyDefaultMapper devicePropertyDefaultMapper;
    
	@Override
	public String syncTaskType(String params) {
		log.info("同步预定义任务类型:"+params);
		JSONObject pmsParamJsonObject = JSON.parseObject(params);
		String pmsBdzId = pmsParamJsonObject.getString("BDZID");
		List<JSONObject> jsonObjects = new ArrayList<>();
		
		//通过pms变电站id获取变电站信息
		SubstationSite station = substationSiteMapper.getStationInfo(pmsBdzId);
		
		if (station!=null) {
			//通过pms变电站id获取任务列表
			List<TaskDef> taskDefs = taskDefMapper.getTaskByPmsBdzId(pmsBdzId);
			for (TaskDef taskDef : taskDefs) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("BDZID", station.getPmsBdzId());
				jsonObject.put("BDZMC", station.getName());
				jsonObject.put("RWLXID", taskDef.getTaskId());
				jsonObject.put("RWLXMC", taskDef.getTaskName());
				jsonObjects.add(jsonObject);
			}
		}
		
		return jsonObjects.toString();
	}

	@Override
	public String sendPatrolZnTask(String params) {
		log.info("下发站内巡检任务:"+params);
//		JSONObject pmsParamJsonObject = JSON.parseObject(params);
//
//		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("zxjg", "0");
//		
//		//检查任务是否存在
//		TaskDef taskDef = taskDefMapper.getTaskByTaskId(Integer.valueOf(pmsParamJsonObject.getString("RWLXID")));
//		
//		if (taskDef!=null) {
//			TaskRegular taskRegular = new TaskRegular();
//			taskRegular.setTaskId(Integer.valueOf(pmsParamJsonObject.getString("RWLXID")));
//			taskRegular.setPmsJhId(pmsParamJsonObject.getString("JHID"));
//			taskRegular.setExecuteTime(pmsParamJsonObject.getDate("JHXSSJ"));
//			taskRegularMapper.insert(taskRegular);
//			sendMsgToUpperComputer(params);
//			jsonObject.put("zxjg", "1");
//		}
		//转发pms的信息给上位机处理
		String response = sendMsgToUpperComputer(params);

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("zxjg", "0");
		
		if (response!="") {
			jsonObject.put("zxjg", "1");
		}

		return jsonObject.toString();
	}
	
	@Override
	@Async
	public void syncTaskStatus(SyncTaskStatusParam syncTaskStatusParam) {
		log.info("同步任务状态:"+JSON.toJSONString(syncTaskStatusParam));
		TaskHistory taskHistory = taskHistoryMapper.getTaskHistoryById(syncTaskStatusParam.getTaskHistoryId());
		if (taskHistory != null) {
			//提交巡视和盘点结果
			savePatrolResult(taskHistory);
			//提交异常数据
			saveException(taskHistory);
		}else {
			log.info("任务执行记录不存在："+syncTaskStatusParam.getTaskHistoryId());
		}
	}

	/**
	 * 提交巡视和盘点结果
	 * @param taskHistory
	 */
	private void savePatrolResult(TaskHistory taskHistory) {
		JSONObject patrolResultJsonObject = new JSONObject();
		patrolResultJsonObject.put("JHID", taskHistory.getPmsJhId());
		
		//获取任务记录下的报警点位数
		int alarmCount = pointAlarmMapper.getAlarmCountByTaskHistoryId(taskHistory.getId());
		if (alarmCount>0) {
			patrolResultJsonObject.put("XSJG", "1");
		}else {
			patrolResultJsonObject.put("XSJG", "0");
		}
		
		//获取实物id列表
		List<Map<String, Object>> swidList = new ArrayList<>();
		List<DevicePropertyDefault> devices = devicePropertyDefaultMapper.getCurrentTaskDeviceList(taskHistory.getId());
		for (DevicePropertyDefault device : devices) {
			Map<String, Object> map = new HashMap<>();
			map.put("XSSBID", device.getPmsDeviceId());
			map.put("XSSWID", device.getPmsEntityId());
			map.put("XSSBMC", device.getDeviceName());
			
			//判断该设备是否有扫描到swid
			int swidNum = powerHistoryMapper.getDevieSwidNum(device.getId(),taskHistory.getId());
			if (swidNum>0) {
				map.put("SFYSWID", "1");
			}else {
				map.put("SFYSWID", "0");
			}
			swidList.add(map);
		}
		patrolResultJsonObject.put("SWID", swidList);
		log.info("提交巡视和盘点结果:"+patrolResultJsonObject.toJSONString());
	}
	
	/**
	 * 提交异常数据
	 * @param taskHistory
	 */
	private void saveException(TaskHistory taskHistory) {
		SubstationSite station = substationSiteMapper.getStationInfoByTaskId(taskHistory.getTaskId());
		List<PointAlarm> pointAlarms = pointAlarmMapper.getPointAlarmByTaskHistoryId(taskHistory.getId());
		WebServiceClient client = new WebServiceClient();
		for (PointAlarm pointAlarm : pointAlarms) {
			JSONObject exceptionJsonObject = new JSONObject();
			exceptionJsonObject.put("JHID", taskHistory.getPmsJhId());
			exceptionJsonObject.put("BDZID", station.getPmsBdzId());
			exceptionJsonObject.put("BDZMC", station.getName());
			exceptionJsonObject.put("YCID", pointAlarm.getId());
			exceptionJsonObject.put("YCMS", pointAlarm.getAlarmDesc());
			exceptionJsonObject.put("BZ", "报警等级:"+pointAlarm.getAlarmLevelId());
			
			//获取swid列表
			List<Map<String, Object>> swidList = new ArrayList<>();
			DevicePropertyDefault device = devicePropertyDefaultMapper.getDeviceByPointId(pointAlarm.getId());
			
			Map<String, Object> swidMap = new HashMap<>();
			swidMap.put("SBID", device.getPmsDeviceId());
			swidMap.put("SBMC", device.getDeviceName());
			swidMap.put("SWID", device.getPmsEntityId());
			swidList.add(swidMap);
			exceptionJsonObject.put("SWID", swidList);
			log.info("提交异常数据"+exceptionJsonObject.toString());
			
			//提交异常附件
			JSONObject attachmentJsonObject = getAttachment(pointAlarm,taskHistory);
			client.req("saveException", exceptionJsonObject);
            client.req("saveAttachment", attachmentJsonObject);
		}
	}
	
	/**
	 * 获取异常附件
	 * @param taskHistory
	 */
	private JSONObject getAttachment(PointAlarm pointAlarm, TaskHistory taskHistory) {
		PowerHistory powerHistory = powerHistoryMapper.getPowerHistoryByPointIdAndTaskHistoryId(pointAlarm.getPowerHistoryRowid(),taskHistory.getId());
		PointDto pointDto = powerHistoryMapper.getPointById(pointAlarm.getPowerHistoryRowid());
		
		JSONObject attachmentJsonObject = new JSONObject();
		attachmentJsonObject.put("YCID", pointAlarm.getId());
		attachmentJsonObject.put("JHID", taskHistory.getPmsJhId());

		String fj = "";
		try {
			//判断该点位的保存类型是红外还是可见光 1:红外 2:可见光
			if (pointDto.getSaveTypeId() == 1 && powerHistory.getFlirPic()!=null) {
				fj = Base64Util.encode(Base64Util.getBytes(resourcePath + powerHistory.getFlirPic()));
			}else if (pointDto.getSaveTypeId() == 2 && powerHistory.getCameraPic() !=null) {
				fj = Base64Util.encode(Base64Util.getBytes(resourcePath + powerHistory.getCameraPic()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		attachmentJsonObject.put("FJ", fj);
		log.info("提交异常附件:"+attachmentJsonObject.toString());
		return attachmentJsonObject;
	}

	/**
     * 发送消息给机器人
     * @param robotId
     */
    private String sendMsgToUpperComputer(String json) {
    		String response = "";
    		ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket socket = context.socket(SocketType.REQ);
        socket.setSendTimeOut(1000);
        socket.setReceiveTimeOut(3000);
        if (socket.connect("tcp://" + upperComputerIp + ":"+upperComputerPort)) {
            socket.send(json.getBytes(), 0);
            byte[] reply = socket.recv(0);
            response = new String(reply);
            log.info("上位机响应结果:" + response);
            socket.close();
        } else {
            log.info("连接上位机ZMQ服务超时");
        }
        
        return response;
    }
}
