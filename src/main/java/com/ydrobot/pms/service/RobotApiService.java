package com.ydrobot.pms.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.alibaba.fastjson.JSONObject;
import com.ydrobot.pms.request.SyncTaskStatusParam;

@WebService(name = "RobotApiService", targetNamespace = "http://pms.ydrobot.com")
@SOAPBinding(style = Style.RPC)
public interface RobotApiService {
	
    /**
     * 同步机器人预定义任务类型
     * @param params
     * @return
     */
    @WebMethod
    String syncTaskType(String params);

    /**
     * 下发站内巡检任务
     * @param params
     * @return
     */
    @WebMethod
    String sendPatrolZnTask(String params);
    
    /**
     * 上位机同步任务状态
     * @param params
     * @return
     */
    @WebMethod
    void syncTaskStatus(SyncTaskStatusParam syncTaskStatusParam);
}
